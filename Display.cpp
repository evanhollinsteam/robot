/**
 * Display.cpp
 * 
 * Evan Hollins - 43987982
 * Ryan Stein - 44901100
 * MECH468
 */

#include "Arduino.h"
#include "Display.hpp"

void Display::init()
{
    lcd.init();
    lcd.backlight();
    lcd.clear();
}

void Display::displayString(String str)
{
    lcd.clear();
    if (str.length() < width * height)
    {
        lcd.setCursor(0, 0); //set cursor to start pos
        lcd.print(str);      //print the text
    }
    else
    {
        scrollString(str); //if larger than screen, we need to scroll.
    }
}

void Display::scrollString(String str)
{
    currentMillis = millis();
    Serial.print("current millis:");
    Serial.println(currentMillis);

    if (currentMillis - lastMillis > SCROLL_DELAY)
    {
        lcd.clear();
        Serial.println("in the if");
        while (str.length() < width * height)
        {               //if we want a scroll but the word is smaller than screen.
            str+=" "; //fill it out
        }
        Serial.println(str);

        lcd.setCursor(0, 0); //reprint
        String line1 = str.substring(0, 15);

        Serial.println(line1);

        lcd.print(line1);
        lcd.setCursor(0, 1);
        lcd.print(str.substring(16, 16 + 15));

        Serial.println("After print?");
        /*
        move the first char to the end.
        */
        String temp = str.substring(1); //inefficent way but
        str = temp + str.charAt(0);
        //record the last time taken.
        lastMillis = currentMillis;
    }
}

