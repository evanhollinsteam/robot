/**
 * Ultrasonic.hpp
 * 
 * Evan Hollins - 43987982
 * Ryan Stein - 44901100
 * MECH468
 */

#ifndef Ultrasonic_hpp
#define Ultrasonic_hpp

#include "Arduino.h"

class Ultrasonic
{
private:
    enum UltrasonicState {
        READING,
        PULSING,
        WAITING
    };
    static const int PULSING_MICROS = 10;
    static const unsigned long WAITING_MICROS = 300000;
    UltrasonicState state;
    int trigger;
    int echo;
    unsigned long lastTime;
    unsigned long pulseInStart;
    bool previousEchoValue;
    int value;

public:
    Ultrasonic(int trigger, int echo) : trigger(trigger),
                                        echo(echo){};

    void init();
    void update();
    int read();
    void echoInterrupt();
};

#endif