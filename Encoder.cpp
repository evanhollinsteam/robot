/**
 * Encoder.cpp
 * 
 * Evan Hollins - 43987982
 * Ryan Stein - 44901100
 * MECH468
 */

#include "Encoder.hpp"

void Encoder::init()
{
    pinMode(pin, INPUT);
}

int Encoder::getCount()
{
    return count;
}

void Encoder::isr()
{
    if (motor->getSpeed() > 0)
    { //add if forwards
        count++;
    }
    else if (motor->getSpeed() < 0)
    {
        count--; //subtract if backwards.
    }
    //don't change count if speed = 0.
}

void Encoder::reset() {
    count = 0;
}
