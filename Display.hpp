/**
 * Display.hpp
 * 
 * Evan Hollins - 43987982
 * Ryan Stein - 44901100
 * MECH468
 */

#ifndef Display_hpp
#define Display_hpp

#include "Arduino.h"
#include <Wire.h>
#include "LiquidCrystal_I2C.h"

#define SCROLL_DELAY 500

class Display
{
private:
    static const int width = 16;
    static const int height = 2;
    LiquidCrystal_I2C lcd; //create the LCD object.
    unsigned long currentMillis = 0;
    unsigned long lastMillis = 0;

public: //Not sure if i did this right.....
    Display(): 
        lcd(0x27, width, height){};
    void init();
    void displayString(String str);
    void scrollString(String str);
};

#endif