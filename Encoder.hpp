/**
 * Encoder.hpp
 * 
 * Evan Hollins - 43987982
 * Ryan Stein - 44901100
 * MECH468
 */

#ifndef Encoder_hpp
#define Encoder_hpp

#include "Arduino.h"
#include "Motor.hpp"

class Encoder
{
private:
    int pin;
    int count;
    Motor *motor;

public:
    Encoder(int pin,
            Motor *motor) : pin(pin), motor(motor){}
    void init();
    int getCount();
    void isr();
    void reset();
};

#endif