/**
 * ShiftRegister.cpp
 * 
 * Evan Hollins - 43987982
 * Ryan Stein - 44901100
 * MECH468
 */

#include "Arduino.h"
#include "ShiftRegister.hpp"

void ShiftRegister::init()
{
    // do any pin mode or digitalWrites here
    //the following setup doesn't utilize a common SDA / SCL pulse. (if we use an LCD latter this will matter)
    pinMode(clock, OUTPUT);
    pinMode(clear, OUTPUT);
    pinMode(data, OUTPUT);
    digitalWrite(clock, LOW);
    digitalWrite(clear, HIGH);
    digitalWrite(data, LOW);
}

/**
* @Param bit, the bit we want to shift in next
* Pre: bit is 1 or 0
**/
void ShiftRegister::shiftBit(int bit)
{
    digitalWrite(clock, LOW); //confirm data/clock 0
    digitalWrite(data, LOW);
    if (bit == 0b1) {
        digitalWrite(data, HIGH);
    } else {
        digitalWrite(data, LOW);
    }
    pulse(clock); //If using a standard clock pulse, This is where we need to change something
}

/**
 * @Param the pin to pulse
 * Pre: pin is digital, pin is low
 **/
void ShiftRegister::pulse(int pin)
{
    digitalWrite(pin, HIGH);
    digitalWrite(pin, LOW);
}

/*
 * @Param an 8 bit digit stored as an int array (Is there a better way to store this in C++?)
 * Shifts in an 8 bit digit to the register
 */
void ShiftRegister::shiftchar(char num)
{
    shiftOut(data, clock, LSBFIRST, num);
}

/**
 * Clears the ShiftRegister
 **/
void ShiftRegister::clearDisplay()
{
    digitalWrite(clear, LOW);
    digitalWrite(clear, HIGH);
}
