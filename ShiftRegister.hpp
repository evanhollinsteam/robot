/**
 * ShiftRegister.cpp
 * 
 * Evan Hollins - 43987982
 * Ryan Stein - 44901100
 * MECH468
 */

#ifndef ShiftRegister_HPP
#define ShiftRegister_HPP

#include "Arduino.h"

class ShiftRegister
{
private:
   int clock;
   int clear;
   int data;
   void shiftBit(int bit);
   void pulse(int pin);
   void clearDisplay();

public:
   ShiftRegister(int clock, int clear, int data) : clock(clock),
                                                   clear(clear),
                                                   data(data)
   {
   }

   void init();
   void shiftchar(char num);
};

#endif