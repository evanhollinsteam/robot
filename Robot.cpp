/**
 * Robot.cpp
 * 
 * Evan Hollins - 43987982
 * Ryan Stein - 44901100
 * MECH468
 */

#include "Arduino.h"
#include "Robot.hpp"

void Robot::init()
{
   ultrasonicServo.attach(ultrasonicServoPin);
   leftMotor->init();
   rightMotor->init();
}

void Robot::driveStraight(float speed)
{
   rightMotor->setSpeed(speed * 0.95);
   leftMotor->setSpeed(speed);
}

void Robot::setRightMotor(float speed) {
   rightMotor->setSpeed(speed);
}

void Robot::setLeftMotor(float speed) {
   leftMotor->setSpeed(speed);
}

void Robot::stop()
{
   driveStraight(0);
}

void Robot::lookAt(int angle)
{
   ultrasonicServo.write(angle);
}

void Robot::setTarget(float _x)
{
   targetDist = _x;
}

void Robot::setRunningToTarget(bool x) {
   running = x;
}

float Robot::getDist() {
  return dist;
}

float Robot::getTarget() {
  return targetDist;
}

void Robot::update()
{
   if (lastUpdateMicros == 0 || micros() - lastUpdateMicros > 20000)
   {
      // Keep track of position
      float SL = leftEnc->getCount() * C;
      float SR = rightEnc->getCount() * C;
      dist += (SR + SL) * 0.5;
      float tempAngle = SR - SL;
      leftEnc->reset();
      rightEnc->reset();
      lastUpdateMicros = micros();

      // Try and go to point
      if (running)
      {
         float error = targetDist - dist;

         if (abs(error) <= DIST_THREASHOLD)
         {
            leftMotor->stop();
            rightMotor->stop();
         }
         else
         {
            error *= P_DIST;
            tempAngle *= P_ANGLE;


            // Factor in MIN_POWER
            if (abs(error) < MIN_POWER)
            {
               error = MIN_POWER * sgn(error);
            } else if (abs(error) > 1.0) {
              error = sgn(error);
            }

            Serial.print("L: ");
            Serial.print(error+tempAngle);
            Serial.print(" R: ");
            Serial.println(error-tempAngle);

            leftMotor->setSpeed(error + tempAngle);
            rightMotor->setSpeed(error - tempAngle);
         }
      }
   }
}
