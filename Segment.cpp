/**
 * 7Seg4Dig.cpp
 * 
 * Evan Hollins - 43987982
 * Ryan Stein - 44901100
 * MECH468
 */

#include "Arduino.h"
#include "Segment.hpp"

void Segment::init()
{
    // do any pin mode or digitalWrites here
    pinMode(digit1, OUTPUT);
    pinMode(digit2, OUTPUT);
    pinMode(digit3, OUTPUT);
    pinMode(digit4, OUTPUT);
    digitalWrite(digit1, LOW);
    digitalWrite(digit2, LOW);
    digitalWrite(digit3, LOW);
    digitalWrite(digit4, LOW);
    shiftRegister->init();
}

/**
 * Turns all digits off 
 **/
void Segment::digitsOff()
{
    digitalWrite(digit1, LOW);
    digitalWrite(digit2, LOW);
    digitalWrite(digit3, LOW);
    digitalWrite(digit4, LOW);
}

/**
 * @Param num = a number between 0 - 9 to display
 *        digit = the digit to display num on
 *         dp, true or false have a decimal point (maybe change to an int?)
 * 
 **/
void Segment::displayDigit(int num, int digit, bool dp)
{
    char binary;

    switch (num)
    { // there might be a better way to do this, but this is okay for now.
        //NOTE: might need to reverse the binary for these (depends on how the shift works best. I think this is fine though?)
    case (0):
        binary = DIGIT_0;
        break;
    case (1):
        binary = DIGIT_1;
        break;
    case (2):
        binary = DIGIT_2;
        break;
    case (3):
        binary = DIGIT_3;
        break;
    case (4):
        binary = DIGIT_4;
        break;
    case (5):
        binary = DIGIT_5;
        break;
    case (6):
        binary = DIGIT_6;
        break;
    case (7):
        binary = DIGIT_7;
        break;
    case (8):
        binary = DIGIT_8;
        break;
    case (9):
        binary = DIGIT_9;
        break;
    }

    if(!dp){
        binary |= 0b10000000; // If dp, set the most significant bit, read last
    }
    

    digitsOff();
    shiftRegister->shiftchar(binary);
}

/**
 * Display a 4 digit number
 **/
void Segment::display4Digits(int num)
{
    currentMicros = micros();
    if (currentMicros - lastMicros >= display_time)
    {
        int toDisplay = num; // change to a float system to use the DP later?

        //CLAMP THE DISPLAY VALUE TO 4 DIGITS
        if (toDisplay > 9999)
            toDisplay = 9999;
        if (toDisplay < 0)
            toDisplay = 0;

        switch (digit)
        {
        case (1):
            toDisplay = toDisplay / 1000;
            displayDigit(toDisplay % 10, digit, false);
            digitalWrite(digit1, HIGH);
            break;
        case (2):
            toDisplay = toDisplay / 100;
            displayDigit(toDisplay % 10, digit, false);
            digitalWrite(digit2, HIGH);
            break;
        case (3):
            toDisplay = toDisplay / 10;
            displayDigit(toDisplay % 10, digit, false);
            digitalWrite(digit3, HIGH);
            break;
        case (4):
            displayDigit(toDisplay % 10, digit, false);
            digitalWrite(digit4, HIGH);
            break;
        }
        digit++;
        if (digit > 4){
            digit = 1;
            if(num < 1000)
                digit = 2;
            if(num < 100)
                digit = 3;
            if(num < 10)
                digit = 4;            
        }
        lastMicros = currentMicros;
    }
}

void Segment::displayFloat(float num)
{
    currentMicros = micros();
    if (currentMicros - lastMicros >= display_time)
    {
        int toDisplay = num; // change to a float system to use the DP later?
        if (toDisplay > 999)//CLAMP THE DISPLAY VALUE TO 4 DIGITS
            toDisplay = 999;
        if (toDisplay < 0)
            toDisplay = 0;

        int decimal = (num - (float) toDisplay + 0.05) * 10; //seperate the decimal

        switch (digit)
        {
        case (1):
            toDisplay = toDisplay / 100;
            displayDigit(toDisplay % 10, digit, false);
            digitalWrite(digit1, HIGH);
            break;
        case (2):
            toDisplay = toDisplay / 10;
            displayDigit(toDisplay % 10, digit, false);
            digitalWrite(digit2, HIGH);
            break;
        case (3):
            displayDigit(toDisplay % 10, digit, true);
            digitalWrite(digit3, HIGH);
            break;
        case (4):
            displayDigit(decimal, digit, false);
            digitalWrite(digit4, HIGH);
            break;
        
        }
        digit++;
        if (digit > 4){
            digit = 1;
            if(num < 100)
                digit = 2;
            if(num < 10)
                digit = 3;         
        }

        lastMicros = currentMicros;
    }
}
