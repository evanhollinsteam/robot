/**
 * Robot.ino
 * 
 * Evan Hollins - 43987982
 * Ryan Stein - 44901100
 * MECH468
 */

#include <Servo.h>
#include "Motor.hpp"
#include "Display.hpp"
#include "Robot.hpp"
#include "IMU.hpp"
#include "Encoder.hpp"
#include "RF24.h"
#include "RF24Network.h"
#include "RF24Mesh.h"
#include <SPI.h>

// Left motor pins
#define LEFT_MOTOR_IN1 9
#define LEFT_MOTOR_IN2 4
#define LEFT_MOTOR_SPEED 5
#define LEFT_ENCODER_PIN 3 //must be an interrupt pin

// Right motor pins
#define RIGHT_MOTOR_IN1 7
#define RIGHT_MOTOR_IN2 8
#define RIGHT_MOTOR_SPEED 6
#define RIGHT_ENCODER_PIN 2 //mus be an interrupt pin

// Ultrasonic pins
#define ULTRASONIC_SERVO 10

//LCD
#define LCD_SCL A4 //not explicitly used, setup in imported code.
#define LCD_SDA A5 //not explicitly used, setup in imported code.

/* NOTE: i2c Addresses
LCD = 0x27
Gravity IMU BNO = 0x28
Gravity IMU BMP = 0x76
*/

//RFID
#define RFID_MO 11
#define RFID_MI 12
#define RFID_SCK 13
#define RFID_CE A2
#define RFID_CS A1
#define NODEID 0

Motor leftMotor(LEFT_MOTOR_IN1, LEFT_MOTOR_IN2, LEFT_MOTOR_SPEED);
Encoder leftEnc(LEFT_ENCODER_PIN, &leftMotor);
Motor rightMotor(RIGHT_MOTOR_IN1, RIGHT_MOTOR_IN2, RIGHT_MOTOR_SPEED);
Encoder rightEnc(RIGHT_ENCODER_PIN, &rightMotor);
IMU imu;
Robot robot(
    &leftMotor, 
    &rightMotor,
    &leftEnc,
    &rightEnc,
    ULTRASONIC_SERVO, 
    &imu
);
Display display;

RF24 radio(RFID_CE, RFID_CS);
RF24Network network(radio);
RF24Mesh mesh(radio, network);

uint32_t displayTimer = 0;
struct payload_t {
  unsigned long ms;
  unsigned long counter;
};


unsigned long lastReading;
char buff[16];
//int display;

void leftEncInterrupt() {
    leftEnc.isr();
}

void rightEncInterrupt() {
    rightEnc.isr();
}

void setup()
{
    Serial.begin(115200);
    robot.init();
    //robot.lookAt(90);
    lastReading = millis();
    attachInterrupt(digitalPinToInterrupt(LEFT_ENCODER_PIN), leftEncInterrupt, CHANGE);
    attachInterrupt(digitalPinToInterrupt(RIGHT_ENCODER_PIN), rightEncInterrupt, CHANGE);

    mesh.setNodeID(NODEID);
    // Connect to the mesh
    Serial.println(F("Connecting to the mesh..."));
    mesh.begin();
    
    display.init();
    display.displayString("Robot Started");
    delay(1000);
}

void loop()
{
  
    mesh.update();
    mesh.DHCP();

    if(network.available()) {
        RF24NetworkHeader header;
        network.peek(header);
        
        uint32_t dat=0;
        switch(header.type){
          // Display the incoming millis() values from the sensor nodes
          case 'M': 
            network.read(header,&dat,sizeof(dat)); 
            robot.setTarget((float) dat);
            robot.setRunningToTarget(true);
            break;
          default: network.read(header,0,0); Serial.println(header.type);break;
        }
    }
    
    if (millis() - lastReading > 100) {
      sprintf(buff, "T: %d C: %d", ((int) robot.getTarget()), ((int) robot.getDist()));
      display.displayString(buff);
      lastReading = millis();
    }

     if(millis() - displayTimer > 5000){
        displayTimer = millis();
        Serial.println(" ");
        Serial.println(F("********Assigned Addresses********"));
         for(int i=0; i<mesh.addrListTop; i++){
           Serial.print("NodeID: ");
           Serial.print(mesh.addrList[i].nodeID);
           Serial.print(" RF24Network Address: 0");
           Serial.println(mesh.addrList[i].address,OCT);
         }
        Serial.println(F("**********************************"));
    }
    robot.update();
}


