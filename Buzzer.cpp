#include "Arduino.h"
#include "Buzzer.hpp"
#include "Pitches.hpp"

void Buzzer::play(unsigned int frequency)
{
    tone(pin, frequency);
}

void Buzzer::stop()
{
    noTone(pin);
}

void Buzzer::init()
{
    pinMode(pin, OUTPUT);
}

void Buzzer::music(int piece)
{
    switch (piece)
    {
    case DARTH_VADER:
        playPiece(notesDV, durasDV, speedDV, lengthDV);
        break;
    case MEGALOVANIA:
        playPiece(notesMegalovania, durasMegalovania, speedMegalovania, lengthMegalovania);
        break;
    }
}

void Buzzer::playPiece(short notes[], short duras[], short speed, short length)
{
    dura = speed / duras[note];
    if (pause % 2 == 0)
    {
        play(notes[note]);
        wait = dura;
    }
    else
    {
        stop();
        wait = dura * 0.3;
    }
    if (millis() - lastMillis >= wait)
    {
        if(pause%2 == 1){
        note = (note + 1) % length;
        }
        pause = (pause + 1) % (length * 2);
        lastMillis = millis();
    }
}

void Buzzer::resetSong(){
    stop();
    note = 0;
    pause = 0;
}
