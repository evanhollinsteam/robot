/**
 * Ultrasonic.cpp
 * 
 * Evan Hollins - 43987982
 * Ryan Stein - 44901100
 * MECH468
 */

#include "Arduino.h"
#include "Ultrasonic.hpp"

void Ultrasonic::init()
{
    pinMode(trigger, OUTPUT);
    pinMode(echo, INPUT);
}

int Ultrasonic::read()
{
    return value;
}

void Ultrasonic::echoInterrupt()
{
    if (digitalRead(echo))
    {
        pulseInStart = micros();
    }
    else
    {
        unsigned long temp = micros() - pulseInStart;
        value = temp * 0.034 / 2;
        lastTime = micros();
        state = WAITING;
    }
}

void Ultrasonic::update()
{
    switch (state)
    {
    case PULSING:
        digitalWrite(trigger, HIGH);
        if (micros() - lastTime > PULSING_MICROS)
        {
            state = READING;
            digitalWrite(trigger, LOW);
            lastTime = micros();
        }
        break;
    case READING:
        break;
    case WAITING:
        if (micros() - lastTime > WAITING_MICROS)
        {
            state = PULSING;
            lastTime = micros();
        }
        break;
    }
}
