/**
 * Encoder.hpp
 * 
 * Evan Hollins - 43987982
 * Ryan Stein - 44901100
 * MECH468
 * 
 * https://learn.sparkfun.com/tutorials/mpu-9250-hookup-guide/all
 */

#ifndef IMU_hpp
#define IMU_hpp

#include "Arduino.h"
#include <Wire.h>

class IMU
{
private:

public:
    IMU() {}
    void init();
    //BMP(&Wire, eSdo_t eSdo);
};

#endif
