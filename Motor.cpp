/**
 * Motor.cpp
 * 
 * Evan Hollins - 43987982
 * Ryan Stein - 44901100
 * MECH468
 */

#include "Arduino.h"
#include "Motor.hpp"

void Motor::init()
{
    pinMode(enable1, OUTPUT);
    pinMode(enable2, OUTPUT);
    pinMode(speedPin, OUTPUT);
    digitalWrite(enable1, LOW);
    digitalWrite(enable2, LOW);
}

void Motor::setSpeed(float speed)
{
    speed = constrain(speed, -1.0, 1.0);
  
    if (speed == 0) {
      stop();
      return;
    }
    
    if (speed > 0) {
        digitalWrite(enable1, LOW);
        digitalWrite(enable2, HIGH);
    } else {
        digitalWrite(enable2, LOW);
        digitalWrite(enable1, HIGH);
    }
    analogWrite(speedPin, round(255 * abs(speed)));
    currSpeed = speed;
        
}

void Motor::stop()
{
    digitalWrite(enable1, LOW);
    digitalWrite(enable2, LOW);
    analogWrite(speedPin, 0);
}

float Motor::getSpeed(){
    return currSpeed;
}
