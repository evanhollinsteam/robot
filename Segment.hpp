/**
 * Segment.hpp
 * 
 * Evan Hollins - 43987982
 * Ryan Stein - 44901100
 * MECH468
 */

#ifndef Segment_HPP
#define Segment_HPP

#include "Arduino.h"
#include "ShiftRegister.hpp"

class Segment
{
private:
    int digit1;
    int digit2;
    int digit3;
    int digit4;
    long currentMicros = 0;   // THIS WILL NEED TO BE CHANGED.
    long lastMicros = 0;      // sim above
    long display_time = 2500; // in US
    int digit = 1;            //the digit being displayed
    void digitsOff();

    static const char DIGIT_0 = 0b01000000;
    static const char DIGIT_1 = 0b01111001;
    static const char DIGIT_2 = 0b00100100;
    static const char DIGIT_3 = 0b00110000;
    static const char DIGIT_4 = 0b00011001;
    static const char DIGIT_5 = 0b00010010;
    static const char DIGIT_6 = 0b00000010;
    static const char DIGIT_7 = 0b01111000;
    static const char DIGIT_8 = 0b00000000;
    static const char DIGIT_9 = 0b00011000;

    ShiftRegister * shiftRegister; //Segments ABCDEFG(DP) == 8BIT BINARY in the ShiftRegister
                                 //alt: use 7 or 8 other pins somehow.

public:
    Segment(
        int digit1,
        int digit2,
        int digit3,
        int digit4,
        ShiftRegister * shiftRegister) : digit1(digit1),
                                       digit2(digit2),
                                       digit3(digit3),
                                       digit4(digit4),
                                       shiftRegister(shiftRegister){};
    void init();
    void display4Digits(int num);
    void displayDigit(int num, int digit, bool dp);
    void displayFloat(float num);
};

#endif
