#ifndef PLAYABLE_HPP
#define PLAYABLE_HPP


class Playable {
    public:
        virtual void play();
};

#endif