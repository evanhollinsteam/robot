/**
 * Robot.hpp
 * 
 * Evan Hollins - 43987982
 * Ryan Stein - 44901100
 * MECH468
 * 
 * Note, robot orientation assumes:
 *      Front of the robot is the wheels
 *      0 degrees is along the x axis
 *      Rotating left is positive angle
 */

#ifndef Robot_hpp
#define Robot_hpp

#include "Arduino.h"
#include <Servo.h>
#include "Util.hpp"
#include "Motor.hpp"
#include "Ultrasonic.hpp"
#include "Buzzer.hpp"
#include "Segment.hpp"
#include "Encoder.hpp"
#include "IMU.hpp"


/* NOTE: i2c Addresses
LCD = 0x27
Gravity IMU = 0x28
Gravity IMU (pt 2) = 0x76
*/

class Robot
{
private:
    Motor *leftMotor;
    Motor *rightMotor;
    Encoder *leftEnc;
    Encoder *rightEnc;
    Ultrasonic *ultrasonic;
    int ultrasonicServoPin;
    Servo ultrasonicServo;
    Buzzer *buzzer;
    Segment *segment;
    IMU *imu;
    static constexpr float B = 130; // Wheel to wheel width in mm TODO measure this
    static constexpr float C = 2.625; // Encoder tick to mm TODO measure this
    static constexpr float P_DIST = 0.1; // Proportional term on distance
    static constexpr float P_ANGLE = 0.1; // Proportional term on angle
    static constexpr float MIN_POWER = 0.2; // Minimum power to make sure we always move if off the target
    static constexpr float DIST_THREASHOLD = 5;
    long lastUpdateMicros;
    bool running;
    float targetDist;

public:
    Robot(
        Motor *_leftMotor,
        Motor *_rightMotor,
        Encoder *_leftEnc,
        Encoder *_rightEnc,
        int _ultrasonicServoPin,
        IMU *_imu) : leftMotor(_leftMotor),
                    leftEnc(_leftEnc),
                    rightMotor(_rightMotor),
                    rightEnc(_rightEnc),
                    ultrasonicServoPin(_ultrasonicServoPin),
                    imu(_imu),
                    dist(0)
    {
        ultrasonicServo = Servo();
    };


    float dist;

    void init();
    void update();
    void driveStraight(float);
    void setRightMotor(float);
    void setLeftMotor(float);
    void lookAt(int angle);
    void stop();
    void setTarget(float _x);
    void setRunningToTarget(bool x);
    float getDist();
    float getTarget();
};

#endif
