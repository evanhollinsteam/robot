/**
 * Motor.hpp
 * 
 * Evan Hollins - 43987982
 * Ryan Stein - 44901100
 * MECH468
 */

#ifndef Motor_hpp
#define Motor_hpp

#include "Arduino.h"

class Motor
{
private:
    int enable1;
    int enable2;
    int speedPin;
    float currSpeed;
public:
    Motor(int enable1, int enable2, int speedPin) : enable1(enable1),
                                                    enable2(enable2),
                                                    speedPin(speedPin),
                                                    currSpeed(0.1)
    {
    }

    void init();
    void stop();
    void setSpeed(float speed);
    float getSpeed();
};

#endif
